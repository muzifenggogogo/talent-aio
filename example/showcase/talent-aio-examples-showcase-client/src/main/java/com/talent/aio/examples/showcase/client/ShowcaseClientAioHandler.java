package com.talent.aio.examples.showcase.client;

import com.talent.aio.client.intf.ClientAioHandler;
import com.talent.aio.common.ChannelContext;
import com.talent.aio.examples.showcase.common.ShowcaseAbsAioHandler;
import com.talent.aio.examples.showcase.common.ShowcasePacket;
import com.talent.aio.examples.showcase.common.ShowcaseSessionContext;
import com.talent.aio.examples.showcase.common.Type;

/**
 * 
 * @author tanyaowu 
 * 2017年3月27日 上午12:18:11
 */
public class ShowcaseClientAioHandler extends ShowcaseAbsAioHandler implements ClientAioHandler<ShowcaseSessionContext, ShowcasePacket, Object>
{
	/** 
	 * 处理消息
	 */
	@Override
	public Object handler(ShowcasePacket packet, ChannelContext<ShowcaseSessionContext, ShowcasePacket, Object> channelContext) throws Exception
	{
		byte[] body = packet.getBody();
		if (body != null)
		{
			String str = new String(body, ShowcasePacket.CHARSET);
			System.out.println("收到消息：" + str);
		}

		return null;
	}

	private static ShowcasePacket heartbeatPacket = new ShowcasePacket(Type.HEART_BEAT_REQ, null);

	/** 
	 * 此方法如果返回null，框架层面则不会发心跳；如果返回非null，框架层面会定时发本方法返回的消息包
	 */
	@Override
	public ShowcasePacket heartbeatPacket()
	{
		return heartbeatPacket;
	}
}
