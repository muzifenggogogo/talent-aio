/**
 * 
 */
package com.talent.aio.common.task;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talent.aio.common.ChannelContext;
import com.talent.aio.common.GroupContext;
import com.talent.aio.common.intf.Packet;
import com.talent.aio.common.maintain.Syns;
import com.talent.aio.common.threadpool.AbstractQueueRunnable;

/**
 * 
 * @author 谭耀武
 * @date 2012-08-09
 * 
 */
public class HandlerRunnable<SessionContext, P extends Packet, R> extends AbstractQueueRunnable<P>
{
	private static final Logger log = LoggerFactory.getLogger(HandlerRunnable.class);

	private ChannelContext<SessionContext, P, R> channelContext = null;

	public HandlerRunnable(ChannelContext<SessionContext, P, R> channelContext, Executor executor)
	{
		super(executor);
		this.setChannelContext(channelContext);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{

	}

	private AtomicLong synFailCount = new AtomicLong();
	
	/**
	 * 处理packet
	 * @param packet
	 * @return
	 *
	 * @author: tanyaowu
	 * @创建时间:　2017年3月22日 下午4:24:24
	 *
	 */
	public int handler(P packet)
	{
		int ret = 0;

		try
		{
			GroupContext<SessionContext, P, R> groupContext = channelContext.getGroupContext();

			Integer synSeq = packet.getSynSeq();
			if (synSeq != null && synSeq > 0)
			{
				Syns<SessionContext, P, R> syns = channelContext.getGroupContext().getSyns();
				P initPacket = syns.remove(synSeq);
				if (initPacket != null)
				{
					synchronized (initPacket)
					{
						syns.put(synSeq, packet);
						initPacket.notify();
					}
					groupContext.getGroupStat().getHandledPacket().incrementAndGet();
				} else
				{
					log.error("[{}]同步消息失败, synSeq is {}, 但是同步集合中没有对应key值", synFailCount.incrementAndGet(), synSeq);
				}
			} else
			{
				groupContext.getAioHandler().handler(packet, channelContext);
				groupContext.getGroupStat().getHandledPacket().incrementAndGet();
			}
			ret++;
		} catch (Exception e)
		{
			log.error(e.toString(), e);
			return ret;
		} finally
		{

		}
	
		return ret;
	}

	

	/**
	 * 清空处理的队列消息
	 */
	public void clearMsgQueue()
	{
		msgQueue.clear();
	}

	public ChannelContext<SessionContext, P, R> getChannelContext()
	{
		return channelContext;
	}

	public void setChannelContext(ChannelContext<SessionContext, P, R> channelContext)
	{
		this.channelContext = channelContext;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append(this.getClass().getSimpleName()).append(":");
		builder.append(channelContext.toString());
		return builder.toString();
	}

	/** 
	 * @see com.talent.aio.common.threadpool.intf.SynRunnableIntf#runTask()
	 * 
	 * @重写人: tanyaowu
	 * @重写时间: 2016年12月5日 下午3:02:49
	 * 
	 */
	@Override
	public void runTask()
	{
		P packet = null;
		while ((packet = msgQueue.poll()) != null)
		{
			handler(packet);
		}
	}
}
