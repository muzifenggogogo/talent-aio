<h2>
  talent-aio: 让天下没有难开发的即时通讯
</h2>

<ol>
	<li><h3>简 介</h3>
		 <strong>talent-aio</strong>是基于jdk实现的极易上手、极稳定、性能超强的即时通讯框架。这里有<a href="https://my.oschina.net/talenttan/blog/863545" target="_blank">资料及问题汇总</a>。
	</li>
	
	<li><h3>小目标</h3>
		<strong>web开发领域，springmvc之外有jfinal；TCP长连接领域，netty之外还有更易用的talent-aio</strong>
	</li>
		 
	<li><h3>应用场景</h3>
		IM、实时监控、推送服务(已用群组方式内置此功能)、RPC等实时通讯类型的场景
	</li>
	
	<li><h3>案 例（现在案例反馈太多，此处仅列举talent-aio开放第一个月内的用户反馈到我这的案例）</h3>
		<ul class="masthead-links" style="font-size:14pt;">
		  <li>
			某网管系统(管理数百台刀片服务器的系统)
		  </li>
		  <li>
			某直播平台(视频直播+聊天)
		  </li>
		  <li>
			某智能设备检测系统(数据采集)<!--小白-->
		  </li>
		  <li>
			某物联网系统(服务端)<!--好像是jackkang-->
		  </li>
		  <li>
			深圳市某在线技术发展有限公司(中银联投资)：某网络安全运营支撑平台<!--小宇-->
		  </li>
		  <li>
			<a href="https://git.oschina.net/websterlu/redisx" target="_blank">redisx</a><!--小宇-->
		  </li>
		  <li>
			<a href="https://git.oschina.net/kangjie1209/talent_dubbo" target="_blank">talent_dubbo</a><!--jackkang-->
		  </li>
		  <li>
			某移动省公司CRM业务受理消息采集平台(数据采集)<!--福州-精灵-java-->
		  </li>
		</ul>
	</li>
	
		
	<li><h3>特 点</h3>
		<ul class="masthead-links" style="">
		  <li>
			<strong>极简洁清晰易懂的API</strong>: 没有生涩难懂的新概念，只需<strong>花上30分钟</strong><a href="http://www.talent-tan.com:9292/quickstart.html" target="_blank">学习helloworld</a>就能很好地掌握并实现一个性能极好的即时通讯应用
		  </li>
		  <li>
			<strong>极震撼的性能</strong>
			<ul>
				<li>
					轻松支持<strong>百万级</strong>tcp长连接，彻底甩开业界<strong>C1000K</strong>烦恼（centos 单CPU4核 16G 测试数据: 17.82万长连接，只消耗800M内存，CPU毫无压力）
				</li>
				<li>
					最高时，每秒可以收发<strong>333.33万</strong>条消息(约<strong>93.33M</strong>)(windows7、i7、8g、群聊场景)
				</li>
			</ul>
		  </li>
		  
		  <li>
			<strong>极亲民的内置功能</strong>
			<ul>
				<li>
					框架层面帮你<strong>检测心跳</strong>(tcp server)、<strong>发送心跳</strong>(tcp client)
				</li>
				<li>
					框架层面支持<strong>自动重连</strong>(可设置重连间隔时间和重连次数)
				</li>
				<li>
					框架层面支持<strong>同步消息</strong>(消息发送后，等到响应消息再往下执行)
				</li>
				<li>
					框架层面支持<strong>绑定userid</strong>(用于用户关联)、<strong>绑定groupid</strong>(用于群聊)
				</li>
				<li>
					内置各项统计功能----接受过多少连接、关闭过多少连接、已发送的消息数、已接收的消息数、当前是多少正常连接、当前多少断开的连接等。
				</li>
			</ul>
		  </li>
		</ul>
	</li>
	
	
	<li><h3>性能数据</h3>
		<ol>
			<li>
				<h4>IM实例收发速度<strong>333万条/秒</strong></h4>
				<a href="https://static.oschina.net/uploads/space/2017/0322/164624_AJv1_175825.png" target='_blank'><img src='https://static.oschina.net/uploads/space/2017/0322/164624_AJv1_175825.png'/></a>
			</li>
			<li>
				<h4>IM实例<strong>17.82万TCP长连接且正常收发消息只消耗800M内存，CPU使用率极低</strong>，目测talent-aio可以支撑<strong>200万长连接</strong></h4>
			</li>
			<li>
				<h4>十几万长连接反复重连断开又重连，服务器内存保持稳定（600多M到900M间）</h4>
			</li>
		</ol>
	</li>
	
	<li><h3>性能测试步骤</h3>
		<table>
			<tr>
				<td>
				<img src='https://git.oschina.net/tywo45/talent-aio/raw/master/docs/step/1.png'>
				</td>
			</tr>
			<tr>
				<td>
				<img  src='https://git.oschina.net/tywo45/talent-aio/raw/master/docs/step/2.png'>
				</img>
				</td>
			</tr>
		</table>
	</li>

	


	<li><h3>talent-aio产生的背景</h3>
		<ol>
			<li>2011年作者参与中兴某刀片的网管系统开发，大领导要求作者改造老的实时通讯模块。于是开始学习nio，改造后的系统，可管理上千个节点，消息收发速度极快，核心代码至今仍然在运行，这便是后来talent-nio的雏形</li>
			<li>2014年从无到有创建热波直播平台，持续优化了talent-nio，有人提议我开源talent-nio</li>
			<li>考虑到talent-nio有历史包袱，于是重新基于aio写了talent-aio，线程池部分和部分思想来源于并优化于talent-nio，在性能大步提升的基础上，易用性得到根本性解决。</li>
		</ol>
	</li>
	
	
	<li><h3>talent-aio学习步骤（供参考，具体步骤根据各人而异）</h3>
		<ol>
			<li><h4>初步认识talent-aio</h4>
				<ol>
					<li>从<a href="https://git.oschina.net/tywo45/talent-aio" target="_blank">https://git.oschina.net/tywo45/talent-aio</a>处下载源代码</li>
					<li>双击install.bat安装talent-aio到本地maven仓库</li>
					<li>双击start-im-server.bat启动im server</li>
					<li>双击start-im-client.bat启动im client</li>
					<li>对着界面把玩几下，对talent-aio形成感性认识</li>
				</ol>
			</li>
			
			<li><h4>花30分钟学习hello world</h4>
				传送门: <a href="http://www.talent-tan.com:9292/quickstart.html" target="_blank">30分钟快乐入门</a>
			</li>
			
			<li><h4>花点时间学习showcase</h4>
				代码正在开发中，文档暂未开始... ...尽量在2017年4月30号前提供，在此之前有问题可以和作者沟通。有什么需求可以在这里反馈给我：
				<a href="https://my.oschina.net/talenttan/tweet/12616527" target="_blank">showcase需求反馈</a>
				
				
				
			</li>
			
		</ol>
		
		
	</li>




	<li><h3>参与talent-aio</h3>
		<ol>
			<li>talent-aio是将多线程技巧运用到极致的框架，所以一旦您参与到本项目，将会从本项目中学到很多关于多线程的技巧。</li>
			<li>
			<a 
			  href="/tywo45/talent-aio/issues/new?issue%5Bassignee_id%5D=&amp;issue%5Bmilestone_id%5D="
			  class="ui mini green button"
			  title="提交issue">
			<i class="icon plus"></i>提交Issue
			</a>
			给项目提出有意义的新需求，或是帮项目发现BUG，或是上传你本地测试的一些数据让作者参考以便进一步优化。
			</li>

			<li>
			点击右上方的
			<span class="basic buttons mini star-container ui">
			<a href="javascritp:void(0);" class="ui button star" data-method="post" data-remote="true" rel="nofollow">Star</a>
			</span>
			以便随时掌握本项目的动态
			</li>
			
			<li>加QQ群交流
				
					<table style='font-family: "Tahoma", "宋体", "Arial", "Verdana";font-size: 12pt;line-height: 18px;border:1px #99BBE8 solid;border-collapse:collapse;padding:5px;'>
						<tr>
							<td style='padding:4px;border:1px #99BBE8 solid;width:300px;font-size: 14pt;font-weight:bolder;' align="center">免费群</td>
							<td style='padding:4px;border:1px #99BBE8 solid;width:300px;font-size: 14pt;font-weight:bolder;' align="center">付费群</td>
							<td style='padding:4px;border:1px #99BBE8 solid;width:300px;font-size: 14pt;font-weight:bolder;' align="center">打赏群</td>
						</tr>
						<tr>
							<td style='padding:4px;border:1px #99BBE8 solid;width:300px;font-size: 11pt;'>对talent-aio有点好奇的先加此群</td>
							<td style='padding:4px;border:1px #99BBE8 solid;width:300px;font-size: 11pt;'>对talent-aio已经了解，并且觉得对自己很有用处，加此群</td>
							<td style='padding:4px;border:1px #99BBE8 solid;width:300px;font-size: 11pt;'>打赏额>=100，有其它需求，或者出于对优秀国产软件支持的，加此群<br>
							
							<strong>打赏传送门:<span style="padding:8px;">
								<a target="_blank" href="https://git.oschina.net/tywo45/talent-aio/raw/master/docs/pay/ali.jpg">马云打赏</a>
							</span>
							<span style="padding:8px;">
								<a target="_blank" href="https://git.oschina.net/tywo45/talent-aio/raw/master/docs/pay/wechat.png">马化腾打赏</a>
							</span>
							</strong>
							</td>
						</tr>
						
						<tr>
							<td style='padding:4px;border:1px #99BBE8 solid;width:300px;font-size: 14pt;font-weight:bolder;' align="center">
								<a target="_blank" href="http://qm.qq.com/cgi-bin/qm/qr?k=I1WTlwY00uKBluFzJd4rj5aNcmQxoCP-">
									311496904<br/>
									<img src='https://git.oschina.net/tywo45/talent-aio/raw/master/docs/qq_group-free-1.png' style='width:200px;height:250px;' width='200px' height='250px'/>
								</a>
							</td>
							<td style='padding:4px;border:1px #99BBE8 solid;width:300px;font-size: 14pt;font-weight:bolder;' align="center">
								<a target="_blank" href="http://qun.qq.com/qunpay/qunfee/qrcode.html?gc=428058412&source=joingroup&_wv=1031">
									428058412<br/>
									<img src='https://git.oschina.net/tywo45/talent-aio/raw/master/docs/qq-group-2.jpg' style='width:200px;height:250px;' width='200px' height='250px'/>
								</a>
							</td>
							<td style='padding:4px;border:1px #99BBE8 solid;width:300px;font-size: 14pt;font-weight:bolder;' align="center">
								<a target="_blank" href="http://qm.qq.com/cgi-bin/qm/qr?k=xinDFQNH_FWHjVj0biDb3hKHrsiOs5zr">
									492677125<br/>
									<img src='https://git.oschina.net/tywo45/talent-aio/raw/master/docs/qq-group-xx-1.png' style='width:200px;height:250px;' width='200px' height='250px'/>
								</a>
							</td>
						</tr>
					
					</table>
			
			</li>
			

			
		</ol>
	</li>
	
	
	
	
	<li><h3>注意事项</h3>
		<ol>
			<li>请不要在issue中提问题，以免影响watch人员，可以在评论中提问</li>
			
		</ol>
	</li>
	
	<li><h3>版本历史</h3>
		<ol>
			<a href="https://www.oschina.net/news/83057/talent-aio-1-6-6" target="_blank">
			<li>
			  <h3>talent-aio1.6.6 发布，再迎民间高手叫阵----代码当众大PK</h3>
			</li>
			</a>
			本次迎来实力强悍的j-net2作者的挑战，500人大群直接PK，现场十分精彩。
			
			<a href="https://www.oschina.net/news/82566/talent-aio-1-0-2" target="_blank">
			<li>
			  <h3>talent-aio 1.0.2 发布，让天下没有难开发的即时通讯</h3>
			</li>
			</a>
			增加多项功能、各种变态测试
			
			
			<a href="https://www.oschina.net/news/81914/talent-aio-1-0-1" target="_blank">
			<li>
			  <h3>talent-aio 1.0.1 发布，更快更稳更好用</h3>
			</li>
			</a>
			直接把每秒收发条数从138万提升到283万，快到令人窒息。
			
			
			
			
			<a href="https://www.oschina.net/news/80985/talent-aio-1-0-0" target="_blank">
			<li>
			  <h3>talent-aio 1.0.0 正式版，千呼万唤始出来</h3>
			</li>
			</a>
			talent-aio第一个正式版
			
			
			
			<a href="https://www.oschina.net/news/80887/talent-aio-0-6-8-alpha" target="_blank">
			<li>
			  <h3>即时通讯框架 talent-aio 0.6.8-alpha 发布</h3>
			</li>
			</a>
			talent-aio第一次发新闻
		</ol>
	</li>
</ol>

<h3>如果talent-aio帮您节约了大量脑力和开发时间，或助你开发了一个牛逼的产品，或让你成为了领导的爱将，或助你往架构师方向小小迈进了一步等等，可以适度捐赠一下，以支持国产精品开源软件更好的发展！</h3>

<h2>talent-aio承诺</h2>
<ol>
	<li><h3>永远基于LGPL协议开源</h3></li>
	<li><h3>代码将毫无保留地开放给世界</h3></li>
	<li><h3>以成为世界一流开源软件为目标，做国产优秀良心作品</h3></li>
	<li><h3>倾听用户需求，快速响应用户反馈</h3></li>
</ol>
<h2>也请大家多支持国产优秀开源作品，这样将产生更多的国产开源精品</h2>



